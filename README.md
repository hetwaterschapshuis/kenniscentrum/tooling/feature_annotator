# feature_annotator
QGIS Plugin for annotating geometric features for Machine-Learning.

## overview
This plugin can be used to manually annotate each feature in a qgis vector layer. 
It allows you to add a new field, add labels and save your progress.
To export the final dataset, use the `feature_dataset_exporter`: https://gitlab.com/hetwaterschapshuis/kenniscentrum/tooling/feature_dataset_exporter.


## Installation

### Manual
Download the repository as a `.zip` file and use the qgis plugin manager (under Plugins -> Manage and Install Plugins) with the option `Install from ZIP`. <br>
Alternatively: <br>
Clone the repository directly to your qgis plugin folder, usually at a location like `/home/<user>/.local/share/QGIS/QGIS3/profiles/default/python/plugins`

### QGIS Python Plugins Repository
Installation from QGIS directly coming in the future

## Usage
1. Select the layer you want to annotate
2. add a new field to store the annotations in or select an existing field
3. If you created a new field, get the labels from the field by clicking get labels from field. If you want to add new layers fill in the label name and click add. 
4. Once you've setup the field and labels click the `Start Labeling` button to start the process
5. select the label from the dropdown menu for each feature and click next to go to the next feature. You can use the goto index to skip ahead to where you left off.

## contributing
Feel free to contribute by submitting a pull request.


### Maintainer
Code is developed and maintained by j.gerbscheid@hetwaterschapshuis.nl
